import unittest

from sources.ranking import (
    _create_token as create_token,
    _factorize as factorize,
    _sanitize_str as sanitize,
)
from sources.ranking import rank


class RelevanceTest(unittest.TestCase):
    def test_less_tag_that_user_input_is_ignored(self):
        tags = "git main"
        user = "git main temp"

        score = rank(user, tags)

        self.assertEqual(score, 0)

    def test_word_count_close_to_user_input_is_more_relevant(self):
        keyword_1 = "git app1 app6"
        keyword_2 = "git app2 app3 app4 app5"

        user_input = "git app"

        score_bigger = rank(user_input, keyword_1)
        score_lower = rank(user_input, keyword_2)

        self.assertGreater(score_bigger, score_lower)

    def test_parametrized_relevance(self):
        data = [
            [
                "git app",
                "git app1",
                "git app2 app3 app4 app5",
            ],
            ["app1", "git app1", "git app2 app3 app4 app5"],
        ]

        for user_input, keyword_bigger, keyword_lower in data:
            with self.subTest():
                score_bigger = rank(user_input, keyword_bigger)
                score_lower = rank(user_input, keyword_lower)
                self.assertGreater(score_bigger, score_lower)


class ConcreteCasesTest(unittest.TestCase):
    def test_non_pertinent_name_do_not_decrease_relevance(self):
        keyword_1 = "evoting git crypto primitive"
        keyword_2 = "evoting git crypto primitive ts"

        user_input = "git crypt pri ts"

        score_lower = rank(user_input, keyword_1)
        score_bigger = rank(user_input, keyword_2)

        self.assertGreater(score_bigger, score_lower)

    def test_non_pertinent_name_decrease_relevance(self):
        keyword_1 = "evoting git crypto primitive"
        keyword_2 = "evoting git crypto primitive ts"

        user_input = "git crypt pri"

        score_bigger = rank(user_input, keyword_1)
        score_lower = rank(user_input, keyword_2)

        self.assertGreater(score_bigger, score_lower)


class FactorizeTest(unittest.TestCase):
    def test_factorize_length(self):
        tags = "a_1 b_2 c_3/d_4 e_5 f_6/g_7/h_8 i_9/j_0".split(" ")

        actual = factorize(tags)

        # 12=2*3*2
        self.assertEqual(len(actual), 12)


class BasicTest(unittest.TestCase):
    def test_maximum_score_is_100(self):
        w_1 = "test asd"
        w_2 = "test asd"

        score = rank(w_1, w_2)

        self.assertEqual(score, 100)

    def test_minimum_score_is_0(self):
        w_1 = "abcd"
        w_2 = "efgh"

        score = rank(w_1, w_2)

        self.assertEqual(score, 0)

    def test_forbidden_chars_are_strip(self):
        user_input = " :test  123? W b/v"
        expected = "test  123 w bv"

        actual = sanitize(user_input)

        self.assertEqual(expected, actual)

        user_input = " :test  123? W b/v"
        expected = "test  123 w b/v"

        actual = sanitize(user_input, "/")

        self.assertEqual(expected, actual)

    def test_token_ignore_spaces(self):
        user_input = " test  mood git  play "
        expected = ["test", "mood", "git", "play"]

        actual = create_token(user_input)

        self.assertEqual(sorted(expected), sorted(actual))


class MultipartTest(unittest.TestCase):

    def test_more_bundled_words_decrease_relevance(self):
        tags = "a_1 b_2 c_3/d_4/e_6"
        user_input_1 = "a_1 d_4 c_3 e_6"
        user_input_2 = "a_1 d_4 c_3"

        score_1 = rank(user_input_1, tags)
        score_2 = rank(user_input_2, tags)

        self.assertLess(score_1, score_2)

    def test_bundled_word_produce_same_score(self):
        tags = "a_1 b_2 c_3/d_4 e_5 f_6/g_7/h_8"
        user_input_1 = "a_1 c_3"
        user_input_2 = "a_1 d_4"
        user_input_3 = "a_1 e_5"

        score_1 = rank(user_input_1, tags)
        score_2 = rank(user_input_2, tags)
        score_3 = rank(user_input_3, tags)

        self.assertEqual(score_1, score_2)
        self.assertEqual(score_1, score_3)


if __name__ == "__main__":
    unittest.main()
