FROM python:alpine3.19
LABEL authors="sudokillnine"

COPY setup.py .

RUN mkdir sources && \
    pip install --no-cache-dir --upgrade pip &&  \
    pip install --no-cache-dir .

COPY sources/ sources/

WORKDIR sources
ENTRYPOINT ["gunicorn", "-w", "1", "-b", "0.0.0.0:5000", "main:app"]
