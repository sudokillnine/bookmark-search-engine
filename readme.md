# Bookmark search engine

This is a tool which allow you to open your bookmark by entering keywords or tags in your browser search bar.

## How to use

Please see [this guide](./how-to-use.md).