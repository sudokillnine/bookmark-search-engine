import logging
import os

import pandas as pd

logger = logging.getLogger("app")

bookmarks_database_location = os.environ.get("BOOKMARKS_DATABASE")
if not bookmarks_database_location:
    raise EnvironmentError("Missing the BOOKMARKS_DATABASE environment variable.")

COLUMN_KEYWORD = "keyword"
COLUMN_LINK = "link"
COLUMN_TAG = "tags"
COLUMN_SCORE = "score"

_bookmarks_database = None
_bookmarks_tag_indexes = None
_bookmarks_keyword_indexes = None


class BookmarkDatabaseError(Exception):
    pass


def reload():
    if not os.path.isfile(bookmarks_database_location):
        raise BookmarkDatabaseError(
            "The provided bookmark database location do not point to a valid location: "
            + bookmarks_database_location
        )

    global _bookmarks_database
    _bookmarks_database = pd.concat(
        pd.read_excel(bookmarks_database_location, na_filter=False, sheet_name=None),
        ignore_index=True,
    )
    _bookmarks_database = _bookmarks_database.loc[
        :, _bookmarks_database.columns.isin([COLUMN_KEYWORD, COLUMN_LINK, COLUMN_TAG])
    ]

    keyword_length = len(_bookmarks_database[COLUMN_KEYWORD].str.strip())
    link_length = len(_bookmarks_database[COLUMN_LINK].str.strip())
    tag_length = len(_bookmarks_database[COLUMN_TAG].str.strip())
    if keyword_length != link_length and keyword_length != tag_length:
        raise BookmarkDatabaseError(
            "The columns of the Excel file have not the same length."
        )

    global _bookmarks_keyword_indexes
    _bookmarks_keyword_indexes = _bookmarks_database[[COLUMN_KEYWORD]]
    _bookmarks_keyword_indexes = _bookmarks_keyword_indexes[
        _bookmarks_keyword_indexes[COLUMN_KEYWORD].str.strip() != ""
    ]

    global _bookmarks_tag_indexes
    _bookmarks_tag_indexes = _bookmarks_database[[COLUMN_TAG]]
    _bookmarks_tag_indexes = _bookmarks_tag_indexes[
        _bookmarks_tag_indexes[COLUMN_TAG].str.strip() != ""
    ]


def find_keyword(user_input):
    matching_keywords = _bookmarks_keyword_indexes[
        _bookmarks_keyword_indexes[COLUMN_KEYWORD].str.contains(
            rf"\b{user_input}\b", regex=True
        )
    ]
    if len(matching_keywords) == 1:
        index = matching_keywords.index[0]
        content = matching_keywords[COLUMN_KEYWORD].iloc[0]
        return [(100, _bookmarks_database.loc[index, COLUMN_LINK], content)]
    elif len(matching_keywords) > 1:
        logger.warning(
            "Several bookmarks have the same keyword. [keyword:%s]",
            matching_keywords[COLUMN_KEYWORD],
        )
        raise BookmarkDatabaseError("Several bookmarks have the same keyword.")


RELEVANT_BOOKMARKS_COUNT = int(os.environ.get("RELEVANT_BOOKMARKS_COUNT", "5"))
RELEVANCE_SCORE_THRESHOLD = int(os.environ.get("RELEVANCE_SCORE_THRESHOLD", "20"))


def rank_tags(
    tag_ranking_function,
    relevance_threshold=RELEVANCE_SCORE_THRESHOLD,
    relevant_bookmarks_count=RELEVANT_BOOKMARKS_COUNT,
):
    scores = _bookmarks_tag_indexes[COLUMN_TAG].apply(tag_ranking_function)
    if relevant_bookmarks_count > 0:
        scores = scores.nlargest(relevant_bookmarks_count)
    if relevance_threshold > 0:
        scores = scores[scores >= relevance_threshold]
    scores.name = COLUMN_SCORE
    result = pd.merge(
        _bookmarks_database[[COLUMN_LINK, COLUMN_TAG]],
        scores.to_frame(),
        left_index=True,
        right_index=True,
    )
    custom_order = [COLUMN_SCORE, COLUMN_LINK, COLUMN_TAG]
    result = result[custom_order]
    return result.values.tolist()


# When the module is loaded, load the database
reload()
