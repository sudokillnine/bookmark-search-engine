from functools import reduce

from rapidfuzz import fuzz

TAGS_SEPARATOR = " "
MULTI_TAGS_SEPARATOR = "/"


def _sanitize_str(str_input, additional_whitelisted_char=[]):
    return "".join(
        [
            c
            for c in str_input.strip()
            if c.isalnum() or c.isspace() or c in additional_whitelisted_char
        ]
    ).lower()


def _create_token(str_input):
    s = [x for x in str_input.split(" ") if x]
    return s


# pass a list of tag and returned a factorized version of all the world containing '/'
def _factorize(input):
    result = [[]]
    for token in input:
        if "/" in token:
            new_factorize = []
            for multipart in token.split("/"):
                for entry in result:
                    new_factorize.append(entry + [multipart])
            result = new_factorize
        else:
            for entry in result:
                entry.append(token)

    return result


def _rank_string(user_input, tag_input):
    # Fall fast if tags are less than user input
    if len(user_input) > len(tag_input):
        return 0

    user_input_scores = {x: 0.0 for x in user_input}
    tag_input_scores = {x: 0.0 for x in tag_input}
    for u in user_input:
        for t in tag_input:
            user_input_scores[u] = max(fuzz.ratio(u, t[: len(u)]), user_input_scores[u])
            tag_input_scores[t] = max(fuzz.ratio(t, u[: len(t)]), tag_input_scores[t])

    user_input_score = reduce(lambda a, b: a + b, user_input_scores.values())
    user_input_score /= len(user_input_scores)  # Normalize in percent
    tag_input_score = reduce(lambda a, b: a + b, tag_input_scores.values())
    tag_input_score /= len(tag_input_scores)  # Normalize in percent

    return (user_input_score + tag_input_score) / 2


def rank(str_user_input, str_tag_input):
    user_input = _create_token(_sanitize_str(str_user_input))
    tag_input = _create_token(_sanitize_str(str_tag_input, "/"))

    # Factorize input base on the words alternative (using the '/' syntax)
    string_score = 0
    for factorized_tag in _factorize(tag_input):
        string_score = max(_rank_string(user_input, factorized_tag), string_score)

    return string_score
