import logging
import os
import urllib.parse

from flask import Flask, request, redirect, render_template

from database import (
    reload as reload_bookmarks_database,
    rank_tags,
    find_keyword,
    BookmarkDatabaseError,
)
from ranking import rank as ranking_function

# Create web server
app = Flask("Bookmarks search engine", template_folder="/sources/templates")


def create_ranking_function(user_input):
    def rank(row):
        return ranking_function(user_input, row)

    return rank


EQUALITY_SCORE_THRESHOLD = int(os.environ.get("EQUALITY_SCORE_THRESHOLD", "10"))


@app.route("/")
def get_most_relevant_bookmark():
    keyword_match = find_keyword(_get_query())
    if keyword_match:
        app.logger.debug("keyword found. [keyword:%s]", keyword_match[0][2])
        return redirect(keyword_match[0][1], code=302)

    scores = rank_tags(create_ranking_function(_get_query()))
    scores.sort(reverse=True)

    # keep only the values which are equal to the highest score
    scores = [
        score
        for score in scores
        if score[0] >= (scores[0][0] - EQUALITY_SCORE_THRESHOLD)
    ]

    app.logger.debug("score list " + str(scores))
    if scores:
        if len(scores) > 1:
            app.logger.info(
                "multiple tags found. [tags:%s]", [score[2] for score in scores]
            )
            try:
                return render_template("multi-match.html", links=scores)
            except Exception as e:
                app.logger.error(e)
        else:
            app.logger.info("tag found. [tags:%s]", scores[0][2])
            return redirect(scores[0][1], code=302)
    else:
        app.logger.info("no relevant match found.")
        return "no relevant match found."


@app.route("/favicon.ico")
def favicon():
    return "no favicon"


@app.route("/debug/matches")
def get_bookmarks_relevance_list():
    app.logger.debug("get relevant matches list.")

    keyword_match = find_keyword(_get_query())
    tag_matches = rank_tags(create_ranking_function(_get_query()), 0)

    result = (keyword_match or []) + (tag_matches or [])
    result.sort(reverse=True)

    return result


@app.route("/debug/reload")
def reload_bookmarks_from_excel():
    reload_bookmarks_database()
    return "Successfully reloaded."


@app.errorhandler(Exception)
def handle_error(e):
    if isinstance(e, BookmarkDatabaseError):
        return str(e)


def _get_query():
    query = urllib.parse.unquote(request.query_string).strip()
    query = query.replace("+", " ")  # requests from chrome has + instead of space
    app.logger.info("received request. [query: %s]", query)
    return query


# Started with gunicorn
if __name__ != "__main__":
    gunicorn_logger = logging.getLogger("gunicorn.error")
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)

# Started directly with python
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
