from setuptools import setup

setup(
    name="bookmark",
    version="1.0",
    packages=["sources"],
    install_requires=[
        "pandas",
        "openpyxl",
        "Flask",
        "rapidfuzz",
        "gunicorn",
    ],
    extras_require={
        "test": [
            "ruff",
        ]
    },
)
