# How to use

## 1. Prerequisites

- You must have docker installed on your machine.
- You must have access to Gitlab registry to download the image.

## 2. Create links database

Create an Excel file based on this template:

| tags                           | keyword | link                   |
|--------------------------------|---------|------------------------|
| google search                  | goo     | https://www.google.com |
| yahoo deprecated search engine |         | https://www.yahoo.com/ |

- The column _tags_ must contain the tags you want to use to match the link.
- The column _keyword_ should contain one word which will instantly match the link.
- The column _link_ must contain the link to redirect the user on when it is matched.

Note that:
- Keywords have priority over tags.
- You can add as many columns as you want, as long you have the three required columns.
- Formulas are possible inside any cell, the calculated value of the cell is used.

### 2.1. Tags format

The tags must be a string composed only of alpha, numeric, space or slash (note the tags are not case-sensitive).

Each word separated by a space is a tag.

For instance `tool regexp checker` may be used as tags for a link pointing to an online regexp validator.

You can separate several words by a slash to mark them as alternative. From the example above:

`tool regexp checker/validator` will be treated like `tool regexp checker` or `tool regexp validator`, pointing to the same link.


## 3. Start the server

### 3.1 On windows

Create a file named `run.bat` and copy this content inside:

```shell
@echo off

REM replace the place holder with the pass of the folder containing your Excel file
set "Excel_FOLDER=<Excel_FOLDER>" 
REM replace the place holder with the name of your Excel file
set "Excel_NAME=<Excel_NAME>" 

set "imageName=registry.gitlab.com/sudokillnine/bookmark-search-engine"
set "containerName=bookmarks-search-engine"

docker ps -a --filter "name=%containerName%" | find /i "%containerName%" > nul
if %errorlevel% equ 0 (
    echo Container %containerName% is present.
    docker stop %containerName% > nul 2>&1
    docker start %containerName%
) else (
    echo Container %containerName% is not present.
    docker run ^
      --name %containerName% ^
      -p 5000:5000 ^
      -v "%Excel_FOLDER%:/resources:ro" ^
      -e BOOKMARKS_DATABASE="/resources/%Excel_NAME%" ^
      -d ^
      %imageName%
)
```

Do not forget to replace the placeholder ! (`<Excel_FOLDER>` and `<Excel_NAME>`)

Then you can add the script to the task manager to run it at startup.


## 4. Setup the browser

### 4.1 Firefox

1. Right-click on the bookmarks toolbar (or any other folder where you want to save the bookmark) and select "New Bookmark..."
2. In the "New Bookmark" dialog, fill in the following details:

    ![firefox-add-bookmark.png](resources%2Ffirefox-add-bookmark.png)
    - Name: bookmark-search-engine
    - URL: http://127.0.0.1:5000/?%s
    - Keyword: the key you want to use as a shortcut to access the search engine (you should use `l`, because the shortcut to select the navigation bar is CTRL+L so it avoids you moving)
3. Click "Add" or "Done" to save the bookmark.

#### 4.1.1 Tool endpoints
 
You may also add two other bookmarks to firefox:

**Debug**
- Name: bookmark-search-engine-debug
- URL: http://127.0.0.1:5000/debug/matches?%s
- Keyword: `ld`

This one shows you how the algorithm ranks your links according to a given input.

**Reload**
- Name: bookmark-search-engine-reload
- URL: http://127.0.0.1:5000/debug/reload
- Keyword: `lr`

This one reloads the Excel file if you modify it when the server is running. It avoids you to restart the server.


## 5. Try it

Now if you enter `l` followed by some existing tags in your Excel file, it should redirect you to the wanted page.

## 6. Customize

You can fine tune the behavior of the server with those env variable you can pass to the docker container:

| Environment variable      | Type | Description                                                                      | Default value |
|---------------------------|------|----------------------------------------------------------------------------------|---------------|
| RELEVANT_BOOKMARKS_COUNT  | int  | Filter the result to keep only the N most ranked elements                        | 5             |
| RELEVANCE_SCORE_THRESHOLD | int  | Filter the result to keep only the elements above the provided score [0-100]     | 20            |
| EQUALITY_SCORE_THRESHOLD  | int  | Distance from the highest score in which other scores are consider equal [0-100] | 10            |