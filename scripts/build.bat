@echo off

REM This script is used to build the project as a docker image from the source

pushd .
set "workDir=%~dp0.."
cd "%workDir%"
set "imageName=registry.gitlab.com/sudokillnine/bookmark/bookmarks-search-engine"

REM if you are behind a proxy, you may add --trusted-host files.pythonhosted.org to pip
docker build -t %imageName% .
popd